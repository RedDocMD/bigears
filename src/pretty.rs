use std::{fmt::Display, ops::Deref};

pub fn box_around_string<T>(original: T) -> String
where
    T: Deref<Target = str> + Display,
{
    let len = original.len();
    format!(
        "{}\n\u{2502}{}\u{2502}\n{}",
        top_line(len),
        original,
        bottom_line(len)
    )
}

fn top_line(len: usize) -> String {
    String::from("\u{250C}") + &"\u{2500}".repeat(len) + "\u{2510}"
}

fn bottom_line(len: usize) -> String {
    String::from("\u{2514}") + &"\u{2500}".repeat(len) + "\u{2518}"
}
