#[macro_use]
extern crate enum_display_derive;

pub mod err;
pub mod header;
mod pretty;
mod read;
mod table;
