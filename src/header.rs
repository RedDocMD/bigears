use colored::*;
use num_derive::FromPrimitive;
use num_traits::FromPrimitive;
use std::{
    fmt::Display,
    io::{Cursor, Read},
    usize,
};

use crate::{
    err::ElfError,
    pretty::box_around_string,
    read::{read_u16, read_u32, read_usize},
    table::{Alignment, Element, Table},
};

pub const IDENT_LEN: usize = 16;
pub const MAGIC_LEN: usize = 4;
pub const PN_XNUM: u16 = 0xffff;
pub const SHN_LORESERVE: u16 = 0xff00;
pub const SHN_UNDEF: u16 = 0;
pub const SHN_XINDEX: u16 = 0xffff;

#[derive(Debug)]
pub struct Header {
    magic: [u8; MAGIC_LEN],
    class: Class,
    data_format: DataFormat,
    version: Version,
    os_abi: OsABI,
    abi_version: u8,
    obj_type: ObjectType,
    arch: Arch,
    entry_addr: usize,
    prog_hdr_off: usize,
    sec_hdr_off: usize,
    flags: u32,
    elf_hdr_size: u16,
    prog_hdr_entry_size: u16,
    prog_hdr_len: Option<u16>,
    sec_hdr_entry_size: u16,
    sec_hdr_len: Option<u16>,
    sec_hdr_str_idx: Option<u16>,
}

impl Display for Header {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "{}", box_around_string("ELF Header".blue().bold()))?;

        let mut table = Table::new();
        table
            .alignment(vec![Alignment::Right, Alignment::Left])
            .row(vec![Element::cell("Class"), Element::cell(self.class)])
            .row(vec![
                Element::cell("Format"),
                Element::cell(self.data_format),
            ])
            .row(vec![Element::cell("ABI"), Element::cell(self.os_abi)])
            .row(vec![
                Element::cell("Object Type"),
                Element::cell(self.obj_type),
            ])
            .row(vec![
                Element::cell("Architecture"),
                Element::cell(self.arch),
            ])
            .row(vec![
                Element::cell("Entry-point address"),
                Element::cell(format!("{:#x}", self.entry_addr)),
            ])
            .row(vec![
                Element::cell("Program Header offset"),
                Element::cell(format!("{:#x}", self.prog_hdr_off)),
            ])
            .row(vec![
                Element::cell("Program Header entry size"),
                Element::cell(self.prog_hdr_entry_size),
            ]);
        if let Some(prog_hdr_len) = self.prog_hdr_len {
            table.row(vec![
                Element::cell("Program Header length"),
                Element::cell(prog_hdr_len),
            ]);
        }
        table
            .row(vec![
                Element::cell("Section Header offset"),
                Element::cell(format!("{:#x}", self.sec_hdr_off)),
            ])
            .row(vec![
                Element::cell("Section Header entry size"),
                Element::cell(self.sec_hdr_entry_size),
            ]);
        if let Some(sec_hdr_len) = self.sec_hdr_len {
            table.row(vec![
                Element::cell("Section Header length"),
                Element::cell(sec_hdr_len),
            ]);
        }
        if let Some(sec_hdr_str_idx) = self.sec_hdr_str_idx {
            table.row(vec![
                Element::cell("Section Header string index"),
                Element::cell(sec_hdr_str_idx),
            ]);
        }
        table.row(vec![
            Element::cell("ELF Header size"),
            Element::cell(self.elf_hdr_size),
        ]);
        write!(f, "{}", table)
    }
}

#[derive(Debug, FromPrimitive, Display, Clone, Copy)]
pub enum Class {
    Unknown = 0,
    Width32,
    Width64,
}

#[derive(Debug, FromPrimitive, Clone, Copy, Display)]
pub enum DataFormat {
    Unknown = 0,
    LittleEndian,
    BigEndian,
}

#[derive(Debug, FromPrimitive, Display, Clone, Copy)]
pub enum Version {
    Unknown = 0,
    Current,
}

#[derive(Debug, FromPrimitive, Display, Clone, Copy)]
pub enum OsABI {
    SysV = 0,
    HPUX,
    NetBSD,
    Linux,
    Solaris,
    FreeBSD,
    TRU64,
    Arm = 97,
    Standalone = 255,
}

#[derive(Debug, FromPrimitive, Display, Clone, Copy)]
pub enum ObjectType {
    Unknown = 0,
    Relocatable,
    Executable,
    Dynamic,
    Core,
}

#[derive(Debug, FromPrimitive, Display, Clone, Copy)]
pub enum Arch {
    Unknown = 0,
    M32 = 1,
    Sparc = 2,
    I386 = 3,
    M68K = 4,
    M88K = 5,
    I860 = 7,
    Mips = 8,
    PARISC = 15,
    Sparc32Plus = 18,
    PowerPC = 20,
    PowerPC64 = 21,
    S390 = 22,
    Arm = 40,
    SH = 42,
    SPARCV9 = 43,
    IA64 = 50,
    X86_64 = 62,
    VAX = 75,
}

pub fn parse_header(data: &[u8]) -> Result<Header, ElfError> {
    let data = &mut Cursor::new(data);
    let mut buf = [0_u8; IDENT_LEN];
    data.read_exact(&mut buf)?;
    const MAGIC: [u8; MAGIC_LEN] = [127, 69, 76, 70];
    if buf[..MAGIC_LEN] != MAGIC {
        return Err(ElfError::Format(String::from("invalid magic number")));
    }
    let class = FromPrimitive::from_u8(buf[4])
        .ok_or(ElfError::Format(String::from("invalid class byte")))?;
    let data_format = FromPrimitive::from_u8(buf[5])
        .ok_or(ElfError::Format(String::from("invalid data format byte")))?;
    let version = FromPrimitive::from_u8(buf[6])
        .ok_or(ElfError::Format(String::from("invalid version byte")))?;
    let os_abi = FromPrimitive::from_u8(buf[7])
        .ok_or(ElfError::Format(String::from("invalid OS ABI byte")))?;

    let obj_type = FromPrimitive::from_u16(read_u16(data, data_format)?)
        .ok_or(ElfError::Format(String::from("invalid object type")))?;

    let arch = FromPrimitive::from_u16(read_u16(data, data_format)?)
        .ok_or(ElfError::Format(String::from("invalid architecture type")))?;

    // Ignore second version field
    read_u32(data, data_format)?;

    let entry_addr = read_usize(data, data_format)?;
    let prog_hdr_off = read_usize(data, data_format)?;
    let sec_hdr_off = read_usize(data, data_format)?;
    let flags = read_u32(data, data_format)?;
    let elf_hdr_size = read_u16(data, data_format)?;
    let prog_hdr_entry_size = read_u16(data, data_format)?;
    let prog_hdr_len = read_u16(data, data_format)?;
    let sec_hdr_entry_size = read_u16(data, data_format)?;
    let sec_hdr_len = read_u16(data, data_format)?;
    let sec_hdr_str_idx = read_u16(data, data_format)?;

    let phnum = if prog_hdr_len >= PN_XNUM {
        None
    } else {
        Some(prog_hdr_len)
    };
    let shnum = if sec_hdr_len >= SHN_LORESERVE {
        None
    } else {
        Some(sec_hdr_len)
    };
    let shstrndx = if sec_hdr_str_idx >= SHN_LORESERVE {
        None
    } else {
        Some(sec_hdr_str_idx)
    };

    Ok(Header {
        magic: MAGIC,
        class,
        data_format,
        version,
        os_abi,
        abi_version: buf[8],
        obj_type,
        arch,
        entry_addr,
        prog_hdr_off,
        sec_hdr_off,
        flags,
        elf_hdr_size,
        prog_hdr_entry_size,
        prog_hdr_len: phnum,
        sec_hdr_entry_size,
        sec_hdr_len: shnum,
        sec_hdr_str_idx: shstrndx,
    })
}
