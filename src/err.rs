use quick_error::quick_error;

quick_error! {
    #[derive(Debug)]
    pub enum ElfError {
        Io(err: std::io::Error) {
            from()
            source(err)
            display("IO error: {}", err)
        }
        Format(mess: String) {
            display("{}", mess)
        }
    }
}
