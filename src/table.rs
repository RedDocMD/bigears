use std::fmt::{self, Display, Formatter};

pub enum Element {
    Cell(String),
    MultiCell(usize, String),
}

impl Element {
    pub fn cell<T: Display>(el: T) -> Self {
        Self::Cell(el.to_string())
    }

    pub fn multi_cell<T: Display>(span: usize, el: T) -> Self {
        Self::MultiCell(span, el.to_string())
    }
}

#[derive(Clone, Copy)]
pub enum Alignment {
    Centre,
    Right,
    Left,
}

pub struct Table {
    elements: Vec<Vec<Element>>,
    columns: Option<usize>,
    col_len: Option<Vec<usize>>,
    alignment: Option<Vec<Alignment>>,
}

impl Table {
    pub fn new() -> Self {
        Self {
            elements: Vec::new(),
            columns: None,
            col_len: None,
            alignment: None,
        }
    }

    pub fn row(&mut self, row: Vec<Element>) -> &mut Self {
        let new_columns = row_cell_len(&row);
        if let Some(columns) = self.columns {
            if columns != new_columns {
                panic!("Table has {} columns, new row has {}", columns, new_columns);
            }
        } else {
            self.columns = Some(new_columns);
        }
        let new_col_len = expand_row_widths(&row);
        if let Some(col_len) = &mut self.col_len {
            for i in 0..col_len.len() {
                let old_len = col_len[i];
                let new_len = new_col_len[i];
                if new_len > old_len {
                    col_len[i] = new_len;
                }
            }
        } else {
            self.col_len = Some(new_col_len);
        }
        if self.alignment.is_none() {
            self.alignment =
                Some(vec![Alignment::Centre].repeat(self.col_len.as_ref().unwrap().len()));
        }
        self.elements.push(row);
        self
    }

    pub fn alignment(&mut self, align: Vec<Alignment>) -> &mut Self {
        self.alignment = Some(align);
        self
    }

    fn row_to_string(&self, idx: usize) -> String {
        let row = &self.elements[idx];
        let mut idx = 0;
        let mut render = String::new();
        let col_len = self.col_len.as_ref().unwrap();
        let alignment = self.alignment.as_ref().unwrap();
        for (i, el) in row.iter().enumerate() {
            let (width, content, alignment) = match el {
                Element::Cell(el) => {
                    let len = col_len[idx];
                    let alignment = alignment[idx];
                    idx += 1;
                    (len, el, alignment)
                }
                Element::MultiCell(num, el) => {
                    let mut len = 0;
                    for i in idx..(idx + num) {
                        len += col_len[i];
                    }
                    idx += num;
                    (len, el, Alignment::Centre)
                }
            };
            let cell_content = match alignment {
                Alignment::Centre => center_element(content, width),
                Alignment::Right => right_element(content, width),
                Alignment::Left => left_element(content, width),
            };
            render += &cell_content;
            if i != row.len() - 1 {
                render += " \u{2502} ";
            }
        }
        render
    }
}

fn center_element(el: &str, width: usize) -> String {
    if el.len() > width {
        unreachable!("string too long (expected {}): {}", width, el);
    }
    let res = width - el.len();
    let left_len = res / 2;
    let right_len = res - left_len;
    let left = " ".repeat(left_len);
    let right = " ".repeat(right_len);
    left + el + &right
}

fn right_element(el: &str, width: usize) -> String {
    if el.len() > width {
        unreachable!("string too long (expected {}): {}", width, el);
    }
    let res = width - el.len();
    let before = " ".repeat(res);
    before + el
}

fn left_element(el: &str, width: usize) -> String {
    if el.len() > width {
        unreachable!("string too long (expected {}): {}", width, el);
    }
    let res = width - el.len();
    let after = " ".repeat(res);
    el.to_owned() + &after
}

fn row_cell_len(row: &[Element]) -> usize {
    row.iter().fold(0, |acc, el| match el {
        Element::Cell(_) => acc + 1,
        Element::MultiCell(n, _) => acc + *n,
    })
}

fn expand_row_widths(row: &[Element]) -> Vec<usize> {
    let mut widths = Vec::new();
    for el in row {
        match el {
            Element::Cell(st) => widths.push(st.len()),
            Element::MultiCell(num, st) => {
                let sub = (st.len() / *num) + 1;
                let mut new_vec = vec![sub; *num];
                widths.append(&mut new_vec);
            }
        }
    }
    widths
}

impl Display for Table {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        for i in 0..self.elements.len() {
            writeln!(f, "{}", self.row_to_string(i))?;
        }
        Ok(())
    }
}
