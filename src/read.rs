use byteorder::ReadBytesExt;
use std::io::{self, Read};

use crate::header::DataFormat;

pub fn read_u8<T: Read>(data: &mut T) -> io::Result<u8> {
    let mut buf = [0_u8; 1];
    data.read_exact(&mut buf)?;
    Ok(buf[0])
}

pub fn read_u16<T: Read>(data: &mut T, fmt: DataFormat) -> io::Result<u16> {
    match fmt {
        DataFormat::Unknown => panic!("cannot handle unknown byte order"),
        DataFormat::LittleEndian => data.read_u16::<byteorder::LittleEndian>(),
        DataFormat::BigEndian => data.read_u16::<byteorder::BigEndian>(),
    }
}

pub fn read_u32<T: Read>(data: &mut T, fmt: DataFormat) -> io::Result<u32> {
    match fmt {
        DataFormat::Unknown => panic!("cannot handle unknown byte order"),
        DataFormat::LittleEndian => data.read_u32::<byteorder::LittleEndian>(),
        DataFormat::BigEndian => data.read_u32::<byteorder::BigEndian>(),
    }
}

pub fn read_u64<T: Read>(data: &mut T, fmt: DataFormat) -> io::Result<u64> {
    match fmt {
        DataFormat::Unknown => panic!("cannot handle unknown byte order"),
        DataFormat::LittleEndian => data.read_u64::<byteorder::LittleEndian>(),
        DataFormat::BigEndian => data.read_u64::<byteorder::BigEndian>(),
    }
}

pub fn read_usize<T: Read>(data: &mut T, fmt: DataFormat) -> io::Result<usize> {
    Ok(match usize::BITS {
        8 => read_u8(data)? as usize,
        16 => read_u16(data, fmt)? as usize,
        32 => read_u32(data, fmt)? as usize,
        64 => read_u64(data, fmt)? as usize,
        _ => unreachable!("unsupported width: {}", usize::BITS),
    })
}
