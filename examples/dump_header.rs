use std::{env, error::Error, fs::File, io::Read};

use bigears::header::parse_header;

fn main() -> Result<(), Box<dyn Error>> {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    let mut file = File::open(filename)?;
    let mut content = Vec::new();
    file.read_to_end(&mut content)?;
    let header = parse_header(&content)?;
    println!("{}", header);
    Ok(())
}
